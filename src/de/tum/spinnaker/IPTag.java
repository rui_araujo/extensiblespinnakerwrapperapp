package de.tum.spinnaker;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Timer;
import java.util.TimerTask;

public class IPTag {

	public final static int IPTAG_CMD = 26; // IPTAG

	private final static byte RC_ARG = (byte) 0x84;
	private final static byte RC_OK = (byte) 0x80;

	private final static int FIRST_POOL_TAG = 4;
	private final static int TAG_FIXED_SIZE = 4; // At bottom of table
	private final static int TAG_NONE = 255;

	private final static int IPTAG_NEW = 0;
	private final static int IPTAG_SET = 1;
	private final static int IPTAG_GET = 2;
	// private final static int IPTAG_CLR = 3;
	private final static int IPTAG_TTO = 4;
	private final static int IPTAG_MAX = 4;

	private static final IPTag[] iptagList = new IPTag[TAG_NONE];
	private static int defaultTagTT0 = 9; // 2.56s = 10ms * (1 << (9-1))

	private final SocketAddress socketAddress;
	private int timeout;
	private int count;
	private boolean transientTag;

	public static void processIPTagCommand(DatagramSocket datagramSocket,
			DatagramPacket packet) throws IOException {
		int arg1 = ByteArrayUtils.byteArrayToInt(packet.getData(), 14);
		int arg2 = ByteArrayUtils.byteArrayToInt(packet.getData(), 18);
		int arg3 = ByteArrayUtils.byteArrayToInt(packet.getData(), 22);
		int op = (arg1 >> 16) & 0xFFFF;
		int tag = arg1 & 255;

		packet.getData()[10] = RC_OK;
		if (op > IPTAG_MAX) {
			packet.getData()[10] = RC_ARG;
		} else if (op == IPTAG_NEW || op == IPTAG_SET) {
			int timeout = (arg2 >> 16) & 15;
			if (timeout != 0)
				timeout = 1 << (timeout - 1);
			int port = arg2 & 0xffff;
			try {
				final IPTag ipTag = new IPTag(new InetSocketAddress(
						InetAddress.getByAddress(ByteBuffer.allocate(4)
								.order(ByteOrder.LITTLE_ENDIAN).putInt(arg3)
								.array()), port), timeout, op == IPTAG_NEW);

				if (iptagList[tag] != null) {
					ipTag.count = iptagList[tag].count;
					if (!ipTag.transientTag) {
						iptagList[tag].transientTag = false;
					}
					if (iptagList[tag].timeout == 0
							|| iptagList[tag].timeout > ipTag.timeout)
						ipTag.timeout = iptagList[tag].timeout;
				}
				iptagList[tag] = ipTag;
			} catch (UnknownHostException e) {
				LogUpdater.get().appendErrorMessage(e.getMessage());
				return;
			}
			packet.getData()[14] = (byte) (tag & 0xFF);// arg1
			packet.getData()[15] = 0;
			packet.getData()[16] = 0;
			packet.getData()[17] = 0;

		} else if (op == IPTAG_GET) {// TODO:unsupported
			packet.getData()[10] = RC_ARG;
		} else if (op == IPTAG_TTO) {
			packet.getData()[14] = (byte) (defaultTagTT0 & 0xFF);// arg1
			packet.getData()[15] = 0;
			packet.getData()[16] = (byte) (iptagList.length & 0xFF);
			packet.getData()[17] = (byte) (TAG_FIXED_SIZE & 0xFF);
			if (arg2 < 16)
				defaultTagTT0 = arg2;

		} else {// IPTAG_CLR
			iptagList[tag] = null;
		}
		datagramSocket.send(packet);
		LogUpdater.get().appendMessage("Sending Reply for the IPTAg command");
	}

	public static void setTag(SocketAddress socketAddress, byte[] sdpPacket) {
		int flags = sdpPacket[2] & 0x00ff;
		if (flags == 0x07)
			return;
		int currentTag = sdpPacket[3] & 0x00ff;
		if (currentTag != TAG_NONE)
			return;
		int timeout = sdpPacket[1] & 0x00ff;
		if (timeout < 0 || timeout > 16) {
			LogUpdater.get().appendErrorMessage(
					"Invalid IPTag timeout. Setting to infinite timeout.");
			timeout = 0;
		} else {
			if (timeout != 0)
				timeout = 1 << (timeout - 1);
		}
		int firstKeyAvailable = -1;
		for (int i = 0; i < iptagList.length; ++i) {
			final IPTag tag = iptagList[i];
			if (tag != null) {
				if (tag.socketAddress.equals(socketAddress)) {
					tag.count++;
					// Update the timeout to increase if the new one is bigger
					if (timeout == 0)
						tag.timeout = 0;
					else {
						if (timeout > tag.timeout)
							tag.timeout = timeout;
					}
					sdpPacket[3] = (byte) i;
					return;
				}
			} else {
				// FIRST_POOL_TAG marks the beggining of the first transient tag
				if (firstKeyAvailable == -1 && i >= FIRST_POOL_TAG)
					firstKeyAvailable = i;
			}
		}
		// if we get to here, we didn't find an IPTag
		if (firstKeyAvailable == -1) {
			LogUpdater.get().appendErrorMessage(
					"No free IP tags. Leaving the field as is");
			return;
		}
		final IPTag tag = new IPTag(socketAddress, timeout, true);
		iptagList[firstKeyAvailable] = tag;
		sdpPacket[3] = (byte) firstKeyAvailable;
	}

	public static SocketAddress getSocketAddress(int tag) {
		if (tag < 0 || tag >= iptagList.length) {
			return null;
		}
		final IPTag ipTag = iptagList[tag];
		if (ipTag != null) {
			final SocketAddress address = ipTag.socketAddress;
			ipTag.count--;
			if (ipTag.transientTag) {
				if (ipTag.count == 0) {
					iptagList[tag] = null;
				}
			}
			return address;
		}
		return null;
	}

	static {
		new Timer().scheduleAtFixedRate(new TimerTask() {
			@Override
			public void run() {
				for (int i = 0; i < iptagList.length; ++i) {
					if (iptagList[i] != null) {
						if (iptagList[i].timeout > 0) {
							iptagList[i].timeout--;
							if (iptagList[i].timeout == 0) {
								LogUpdater.LOGGER.warning("IPtag "
										+ iptagList[i].socketAddress
										+ "has timed out.");
								iptagList[i] = null;
							}
						}
					}
				}

			}
		}, 10, 10);
	}

	private IPTag(SocketAddress socketAddress, int timeout, boolean transientTag) {
		this.socketAddress = socketAddress;
		this.timeout = timeout;
		this.transientTag = transientTag;
		count = 1;
	}

}
