package de.tum.spinnaker;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.ParseException;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.text.MaskFormatter;

import de.tum.spinnaker.PipeThread.PipeThreadCallbacks;
import de.tum.spinnaker.UARTUtils.PortAttribute;
import de.tum.spinnaker.UARTUtils.PortIdentifier;

public class MainWindows implements PipeThreadCallbacks {
	private final JFrame frame;
	private final JTextPane loggerArea;
	private final JButton connectButton;
	private final JButton scanButton;
	private final JLabel uartComboBoxLabel;
	private final JComboBox<PortIdentifier> uartComboBox;
	private final JLabel baudRateComboBoxLabel;
	private final JComboBox<PortAttribute> baudRateComboBox;

	private final static String[] PACKET_LIST = { "Multicast Packet",
			"P2P packet", "Nearest-neighbour packet", "Fixed-Route packet",
			"Peek/Poke packet" };

	/* Packet Panel components */
	private final JPanel packetPanel;
	private final JButton sendButton;
	private final JFormattedTextField keyTextField;
	private final JFormattedTextField payloadTextField;
	private final JComboBox<String> packetComboBox;
	private final JCheckBox withPayloadCheckbox;
	private final JLabel packetComboBoxLabel;
	private final JLabel keyLabel;
	private final JLabel payloadLabel;

	private boolean hasServerStarted = false;
	private PipeThread pipeThread;

	private void changeUI(boolean newState) {
		uartComboBox.setEnabled(newState);
		baudRateComboBox.setEnabled(newState);
		baudRateComboBoxLabel.setEnabled(newState);
		uartComboBoxLabel.setEnabled(newState);
	}

	private void changePacketManagerUI(boolean newState) {
		packetPanel.setEnabled(newState);
		sendButton.setEnabled(newState);
		keyTextField.setEnabled(newState);
		payloadTextField.setEnabled(newState);
		packetComboBox.setEnabled(newState);
		payloadLabel.setEnabled(newState);
		keyLabel.setEnabled(newState);
		packetComboBoxLabel.setEnabled(newState);
		withPayloadCheckbox.setEnabled(newState);

	}

	public MainWindows() throws ParseException {
		sendButton = new JButton("Send");
		sendButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (pipeThread == null)
					return;
				final long address = Long.decode(keyTextField.getValue()
						.toString());
				final long payload = Long.decode(payloadTextField.getValue()
						.toString());
				switch (packetComboBox.getSelectedIndex()) {
				case 0:
					if (withPayloadCheckbox.isSelected()) {
						pipeThread.sendPacket(CommandProtocol.MC_PAYLOAD,
								address, payload);
					} else {
						pipeThread.sendPacket(CommandProtocol.MC, address);
					}
					break;
				case 1:
					if (withPayloadCheckbox.isSelected()) {
						pipeThread.sendPacket(CommandProtocol.P2P_PAYLOAD,
								address, payload);
					} else {
						pipeThread.sendPacket(CommandProtocol.P2P, address);
					}
					break;
				case 2:
					if (withPayloadCheckbox.isSelected()) {
						pipeThread.sendPacket(CommandProtocol.NN_PAYLOAD,
								address, payload);
					} else {
						pipeThread.sendPacket(CommandProtocol.NN, address);
					}
					break;
				case 3:
					if (withPayloadCheckbox.isSelected()) {
						pipeThread.sendPacket(
								CommandProtocol.FIXED_ROUTE_PAYLOAD, address,
								payload);
					} else {
						pipeThread.sendPacket(CommandProtocol.FIXED_ROUTE,
								address);
					}
					break;
				case 4:
					if (withPayloadCheckbox.isSelected()) {
						pipeThread.sendPacket(CommandProtocol.POKE, address,
								payload);
					} else {
						pipeThread.sendPacket(CommandProtocol.PEEK, address);
					}
					break;
				default:
					break;
				}
				;
			}
		});

		final MaskFormatter formatter = new MaskFormatter("0xHHHHHHHH");
		withPayloadCheckbox = new JCheckBox("With Payload");
		withPayloadCheckbox.setSelected(true);
		withPayloadCheckbox.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (withPayloadCheckbox.isSelected()) {
					payloadTextField.setEnabled(true);
					payloadLabel.setEnabled(true);
				} else {
					payloadTextField.setEnabled(false);
					payloadLabel.setEnabled(false);
				}
			}
		});
		keyTextField = new JFormattedTextField(formatter);
		keyTextField.setValue("0x00000000");
		payloadTextField = new JFormattedTextField(formatter);
		payloadTextField.setValue("0x00000000");
		packetComboBox = new JComboBox<String>(PACKET_LIST);
		packetPanel = new JPanel(new GridLayout(0, 2, 8, 2));

		packetComboBoxLabel = new JLabel("Packet type:");
		packetPanel.setBorder(BorderFactory.createCompoundBorder(
				BorderFactory.createTitledBorder("Packet Manager"),
				BorderFactory.createEmptyBorder(5, 5, 5, 5)));
		packetPanel.add(packetComboBoxLabel);
		packetPanel.add(packetComboBox);
		keyLabel = new JLabel("Routing Key:");
		payloadLabel = new JLabel("Optional Payload:");
		packetPanel.add(keyLabel);
		packetPanel.add(payloadLabel);
		packetPanel.add(keyTextField);
		packetPanel.add(payloadTextField);
		packetPanel.add(sendButton);
		packetPanel.add(withPayloadCheckbox);
		changePacketManagerUI(false);

		loggerArea = new JTextPane();
		LogUpdater.init(loggerArea.getStyledDocument());
		loggerArea.setEditable(false);
		final JScrollPane areaScrollPane = new JScrollPane(loggerArea);
		areaScrollPane
				.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		areaScrollPane
				.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		areaScrollPane.getVerticalScrollBar().setUnitIncrement(32);
		areaScrollPane.setPreferredSize(new Dimension(250, 250));
		areaScrollPane.setBorder(BorderFactory.createCompoundBorder(
				BorderFactory.createCompoundBorder(
						BorderFactory.createTitledBorder("Log"),
						BorderFactory.createEmptyBorder(5, 5, 5, 5)),
				areaScrollPane.getBorder()));

		connectButton = new JButton("Connect");
		connectButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent event) {
				if (!hasServerStarted) {
					final String portName = (String) (((PortIdentifier) (uartComboBox
							.getSelectedItem())).getID());
					pipeThread = new PipeThread(portName,
							(PortAttribute) baudRateComboBox.getSelectedItem(),
							MainWindows.this);
					pipeThread.start();
					hasServerStarted = true;
					connectButton.setText("Disconnect");
					changeUI(false);
					scanButton.setEnabled(false);
					changePacketManagerUI(true);

				} else {
					uartComboBox.removeAllItems();
					baudRateComboBox.removeAllItems();
					pipeThread.stopRequested();
					connectButton.setText("Connect");
					hasServerStarted = false;
					changePacketManagerUI(false);
					scanButton.setEnabled(true);
				}
			}
		});

		scanButton = new JButton("Scan");
		scanButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				uartComboBox.removeAllItems();
				baudRateComboBox.removeAllItems();
				// add available COM ports to menu
				try {
					List<PortIdentifier> lid = UARTUtils
							.getPortIdentifierList();
					if (lid.size() == 0) {
						LogUpdater.get().appendErrorMessage(
								"No UART ports detected");
						changeUI(false);
						connectButton.setEnabled(false);
						return;
					}
					for (PortIdentifier id : lid) {
						uartComboBox.addItem(id);
					}
					uartComboBox.setSelectedIndex(0);

					List<PortAttribute> lpa = UARTUtils.getPortAttributeList();
					for (PortAttribute pa : lpa) {
						baudRateComboBox.addItem(pa);
					}
					baudRateComboBox.setSelectedIndex(0);
					changeUI(true);
					connectButton.setEnabled(true);
				} catch (Error ex) {
					LogUpdater.get().appendErrorMessage(
							"No UART library available");
					uartComboBox.setEnabled(false);
					baudRateComboBox.setEnabled(false);
					connectButton.setEnabled(false);
				}

			}
		});
		uartComboBoxLabel = new JLabel("UART List:");
		uartComboBox = new JComboBox<PortIdentifier>();
		baudRateComboBoxLabel = new JLabel("Baud rate:");
		baudRateComboBox = new JComboBox<PortAttribute>();
		changeUI(false);
		connectButton.setEnabled(false);
		JPanel connectionPanel = new JPanel(new GridLayout(0, 2, 4, 2));
		// final JLabel baudRateComboBoxLabel = new JLabel("Baud rate:");
		connectionPanel.add(uartComboBoxLabel);
		connectionPanel.add(uartComboBox);
		connectionPanel.add(baudRateComboBoxLabel);
		connectionPanel.add(baudRateComboBox);
		connectionPanel.add(scanButton);
		connectionPanel.add(connectButton);
		connectionPanel.setBorder(BorderFactory.createCompoundBorder(
				BorderFactory.createTitledBorder("Connection Manager"),
				BorderFactory.createEmptyBorder(5, 5, 5, 5)));
		frame = new JFrame("SpiNNaker Wrapper");
		JPanel contentPanel = new JPanel(new GridLayout(0, 2));
		contentPanel.add(connectionPanel);
		contentPanel.add(packetPanel);
		frame.getContentPane().add(contentPanel, BorderLayout.NORTH);
		frame.getContentPane().add(areaScrollPane, BorderLayout.CENTER);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(700, 450);
		frame.setLocationRelativeTo(null);
	}

	private void setVisible(boolean visible) {
		frame.setVisible(visible);
	}

	public static void main(String[] args) throws ParseException {
		final MainWindows window = new MainWindows();
		window.setVisible(true);
	}

	@Override
	public void onSerialPortError() {
		uartComboBox.removeAllItems();
		baudRateComboBox.removeAllItems();
		pipeThread.stopRequested();
		connectButton.setText("Connect");
		hasServerStarted = false;
		changePacketManagerUI(false);
		scanButton.setEnabled(true);
	}

}
