package de.tum.spinnaker;

import gnu.io.CommPortIdentifier;
import gnu.io.SerialPort;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Vector;

public class UARTUtils {

	public static class PortIdentifier {
		Object id;
		String display;

		PortIdentifier(Object id, String display) {
			this.id = id;
			this.display = display;
		}

		public String toString() {
			return (display);
		}

		public Object getID() {
			return (id);
		}
	}

	public static class PortAttribute {
		Object o;
		String display;

		PortAttribute(Object o, String display) {
			this.o = o;
			this.display = display;
		}

		public String toString() {
			return (display);
		}

		public Object getO() {
			return (o);
		}
	}

	static public List<PortIdentifier> getPortIdentifierList() {
		CommPortIdentifier portId;
		final ArrayList<PortIdentifier> serPortList = new ArrayList<PortIdentifier>();
		@SuppressWarnings("unchecked")
		final Enumeration<CommPortIdentifier> portList = CommPortIdentifier
				.getPortIdentifiers();

		while (portList.hasMoreElements()) {
			portId = (CommPortIdentifier) portList.nextElement();

			if (portId.getPortType() == CommPortIdentifier.PORT_SERIAL) {
				serPortList.add(new PortIdentifier(portId.getName(), "  "
						+ portId.getName()));
			}
		}
		return serPortList;
	}

	static public Vector<PortAttribute> getPortAttributeList() {
		final Vector<PortAttribute> pa = new Vector<PortAttribute>();
		// pa.add(new PortAttribute(new Integer(12000000), "  12   Mbps"));
		// pa.add(new PortAttribute(new Integer(10000000), "  10 Mbps"));
		pa.add(new PortAttribute(new Integer(9000000), "  9 Mbps"));
		// pa.add(new PortAttribute(new Integer(8000000), "  8   Mbps"));
		// pa.add(new PortAttribute(new Integer(7000000), "  7   Mbps"));
		pa.add(new PortAttribute(new Integer(6000000), "  6   Mbps"));
		// pa.add(new PortAttribute(new Integer(5000000), "  5   Mbps"));
		pa.add(new PortAttribute(new Integer(3000000), "  3   Mbps"));
		pa.add(new PortAttribute(new Integer(2000000), "  2   Mbps"));
		pa.add(new PortAttribute(new Integer(1000000), "  1   Mbps"));
		pa.add(new PortAttribute(new Integer(921600), "  921.6 Kbps"));
		pa.add(new PortAttribute(new Integer(500000), "  500    Kbps"));
		pa.add(new PortAttribute(new Integer(460800), "  460.8 Kbps"));
		pa.add(new PortAttribute(new Integer(250000), "  250    Kbps"));
		pa.add(new PortAttribute(new Integer(230400), "  230.4 Kbps"));
		pa.add(new PortAttribute(new Integer(115200), "  115.2 Kbps"));
		pa.add(new PortAttribute(new Integer(57600), "  57.6 Kbps"));
		pa.add(new PortAttribute(new Integer(38400), "  38.4 Kbps"));
		pa.add(new PortAttribute(new Integer(19200), "  19.2 Kbps"));
		pa.add(new PortAttribute(new Integer(9600), "  9.6 Kbps"));
		pa.add(new PortAttribute(new Integer(2400), "  2.4 Kbps"));
		pa.add(new PortAttribute(new Integer(1200), "  1.2 Kbps"));
		pa.add(new PortAttribute(new Integer(600), "  600      bps"));
		pa.add(new PortAttribute(new Integer(300), "  300      bps"));
		return (pa);
	}

	@SuppressWarnings("unchecked")
	public static SerialPort open(String portName, PortAttribute pa) {
		CommPortIdentifier portId = null;
		Enumeration<CommPortIdentifier> portList;
		SerialPort uartPort;
		boolean portFoundFlag = false;

		portList = CommPortIdentifier.getPortIdentifiers();
		while ((portList.hasMoreElements()) && (portFoundFlag == false)) {

			portId = (CommPortIdentifier) portList.nextElement();
			if (portId.getPortType() == CommPortIdentifier.PORT_SERIAL) {
				if (portId.getName().equals(portName)) {
					portFoundFlag = true;
				}
			}
		}

		if (portFoundFlag == false) {
			return null;
		}

		try { // try opening the port, wait at most 50ms to get port
			uartPort = (SerialPort) portId.open("JavaRS232Port", 50);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

		int baudRate = (Integer) pa.getO();
		try { // set parameters
				// uartPort.enableReceiveThreshold(256);
			uartPort.setSerialPortParams(baudRate, SerialPort.DATABITS_8,
					SerialPort.STOPBITS_1, SerialPort.PARITY_NONE);

			// TODO:get this working on Linux
			if (!System.getProperty("os.name").toLowerCase().contains("linux")) {
				uartPort.setFlowControlMode(SerialPort.FLOWCONTROL_RTSCTS_OUT
						| SerialPort.FLOWCONTROL_RTSCTS_IN);
				uartPort.setRTS(true);
				// uartPort.enableReceiveFraming(512);
				// uartPort.enableReceiveTimeout(4);
			}
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return uartPort; // here we have our port :-)
	}

	private UARTUtils() {
	}
}