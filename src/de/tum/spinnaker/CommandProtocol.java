package de.tum.spinnaker;

public class CommandProtocol {
	// Char received from the uC when receiving a packet
	public static final char RECEIVE_PACKET = 'p';
	public static final char RECEIVE_PACKET_PAYLOAD = 'P';
	public static final char SENT_PACKET_PAYLOAD = 'S';
	public static final char RECEIVE_SDP = 's';

	//Chars sento to the uC before sending a packet
	public static final char BOOT = 'b';
	public static final char P2P_ADDRESS = 'a';
	public static final char SDP = 's';
	public static final char PEEK = 'e';
	public static final char POKE = 'E';
	public static final char P2P = 'p';
	public static final char P2P_PAYLOAD = 'P';
	public static final char FIXED_ROUTE = 'f';
	public static final char FIXED_ROUTE_PAYLOAD = 'F';
	public static final char NN = 'n';
	public static final char NN_PAYLOAD = 'N';
	public static final char MC = 'm';
	public static final char MC_PAYLOAD = 'M';
	public static final char END_CMD = '\n';
}
