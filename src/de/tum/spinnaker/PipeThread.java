package de.tum.spinnaker;

import gnu.io.SerialPort;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketAddress;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

import de.tum.spinnaker.UARTUtils.PortAttribute;

public class PipeThread extends StoppableThread {

	private final static boolean DEBUG = true;

	private final int P2P_ADDRESS = 0x0100;// When connected to link 0
	// private final int P2P_ADDRESS = 0x0000;// When connected to link 3

	private final static long NN_RTRC_KEY = 0x01000000;
	private final static long NN_RTRC_PAYLOAD = 0x00FE0010;
	private final static long NN_P2PC_KEY = 0x053e0000;
	private final static long NN_P2PC_PAYLOAD = 0x02010000;
	private final static long NN_P2PB_KEY = 0x093e1000;
	private final static long NN_P2PB_PAYLOAD = 0x00001001;

	private final static int BOOT_PORT = 54321;
	private final static int SDP_PORT = 17893;
	private final static int MINIMUM_PACKET_SIZE = 26;
	private final static int MAXIMUM_PACKET_SIZE = 282;
	private final String portName;
	private final PortAttribute baudRate;
	private final PipeThreadCallbacks listener;
	private final BlockingQueue<DatagramPacket> packetReceived;
	private final BlockingQueue<Byte> bytesReceived;
	private SerialPort serialPort;
	private SerialInputThread serialInputThread;
	private InputProcessingThread inputThread;
	private SerialOutputThread outputThread;
	private BootOutputThread bootThread;
	private DatagramSocket datagramSocket;
	private final Object serialPortOutputlock;

	private long lastSDPPacket = -1;

	public PipeThread(String portName, PortAttribute baudRate,
			PipeThreadCallbacks listener) {
		serialPortOutputlock = new Object();
		this.portName = portName;
		this.baudRate = baudRate;
		this.listener = listener;
		packetReceived = new ArrayBlockingQueue<>(1000, true);
		bytesReceived = new ArrayBlockingQueue<Byte>(10 * 1024 * 1024, true);
	}

	public void sendPacket(char type, long address) {
		address &= 0xffffffff;
		synchronized (serialPortOutputlock) {
			try {
				final OutputStream serialOutput = serialPort.getOutputStream();
				serialOutput.write(type);
				serialOutput.write(
						ByteBuffer.allocate(8).order(ByteOrder.LITTLE_ENDIAN)
								.putLong(address).array(), 0, 4);
			} catch (IOException e) {
				e.printStackTrace();
				LogUpdater.get().appendErrorMessage(
						"Error while writing packet to the Serial Port.");
				return;
			}
		}
		final String packetString = getPacketString(type, address);
		if (DEBUG)
			LogUpdater.get().appendMessage(packetString);
		else
			LogUpdater.LOGGER.config(packetString);
	}

	public void sendPacket(char type, long address, long payload) {
		address &= 0xffffffffL;
		payload &= 0xffffffffL;
		synchronized (serialPortOutputlock) {
			try {
				final OutputStream serialOutput = serialPort.getOutputStream();
				serialOutput.write(type);
				serialOutput.write(
						ByteBuffer.allocate(8).order(ByteOrder.LITTLE_ENDIAN)
								.putLong(address).array(), 0, 4);
				serialOutput.write(
						ByteBuffer.allocate(8).order(ByteOrder.LITTLE_ENDIAN)
								.putLong(payload).array(), 0, 4);
			} catch (IOException e) {
				e.printStackTrace();
				LogUpdater.get().appendErrorMessage(
						"Error while writing packet to the Serial Port.");
				return;

			}
		}
		final String packetString = getPacketString(type, address, payload);
		if (DEBUG)
			LogUpdater.get().appendMessage(packetString);
		else
			LogUpdater.LOGGER.config(packetString);
	}

	@Override
	public void run() {
		try {
			datagramSocket = new DatagramSocket(SDP_PORT);

			serialPort = UARTUtils.open(portName, baudRate);
			if (serialPort == null) {
				listener.onSerialPortError();
				stopRequested();
				return;
			}
			bootThread = new BootOutputThread();
			bootThread.start();

			outputThread = new SerialOutputThread();
			outputThread.start();

			inputThread = new InputProcessingThread();
			inputThread.start();

			serialInputThread = new SerialInputThread();
			serialInputThread.start();

			while (!stopRequested) {
				// Create a buffer to read datagrams into. If a
				// packet is larger than this buffer, the
				// excess will simply be discarded!
				// 1472 should be the maximum value for boot packets
				// Create a packet to receive data into the buffer
				final DatagramPacket packet = new DatagramPacket(
						new byte[MAXIMUM_PACKET_SIZE], MAXIMUM_PACKET_SIZE);
				datagramSocket.receive(packet);
				packetReceived.put(packet);
			}
		} catch (Exception e) {
			if (!stopRequested)
				e.printStackTrace();
			if (checkIfSerialError(e)) {
				LogUpdater
						.get()
						.appendErrorMessage(
								"Error while writing to the Serial Port. Disconnecting.");
			}
			stopRequested();
			listener.onSerialPortError();
			return;
		} finally {
			serialPort.close();
			datagramSocket.close();
		}
	}

	@Override
	public void stopRequested() {
		if (outputThread != null)
			outputThread.stopRequested();
		if (serialInputThread != null)
			serialInputThread.stopRequested();
		if (inputThread != null)
			inputThread.stopRequested();
		if (bootThread != null)
			bootThread.stopRequested();
		if (serialPort != null)
			serialPort.close();
		if (datagramSocket != null)
			datagramSocket.close();
		super.stopRequested();
	}

	private class SerialOutputThread extends StoppableThread {

		@Override
		public void run() {
			try (final OutputStream serialOutput = serialPort.getOutputStream()) {

				final byte[] p2pSourceAddress = ByteBuffer.allocate(4)
						.order(ByteOrder.LITTLE_ENDIAN).putInt(P2P_ADDRESS)
						.array();
				while (!stopRequested) {
					final DatagramPacket packet = packetReceived.take();
					if (packet.getLength() < MINIMUM_PACKET_SIZE) {
						LogUpdater.get().appendErrorMessage(
								"Received invalid UDP packet. Ignoring.");
						continue;
					}
					int flags = packet.getData()[2] & 0x00ff;
					if (!(flags == 0x87 || flags == 0x07)) {
						LogUpdater.get().appendErrorMessage(
								"Received invalid SDP packet. Ignoring.");
						continue;
					}

					LogUpdater.get().appendMessage(
							"Received a SDP packet from "
									+ packet.getSocketAddress());
					final String sdpString = getSDPString(packet.getData(),
							packet.getLength());
					if (DEBUG)
						LogUpdater.get().appendMessage(sdpString);
					else
						LogUpdater.LOGGER.config(sdpString);
					int cmdRC = ByteArrayUtils.byteArrayToShort(
							packet.getData(), 10);
					if (cmdRC == IPTag.IPTAG_CMD) {
						int srcAddress = ByteArrayUtils.byteArrayToShort(
								packet.getData(), 6);
						if (srcAddress == P2P_ADDRESS) {
							IPTag.processIPTagCommand(datagramSocket, packet);
						} else {
							LogUpdater
									.get()
									.appendMessage(
											"Dropping IPTAG command because it doesn't match our P2P address");
						}
						continue;
					}
					IPTag.setTag(packet.getSocketAddress(), packet.getData());
					// Setting the source CPU to 0 if it is set to the
					// Ethernet as it will be
					int srcCPU = packet.getData()[5] & 0xff;
					if (srcCPU == 0xFF)
						packet.getData()[5] = 0;
					// Setting the source address to the source
					packet.getData()[8] = p2pSourceAddress[0];
					packet.getData()[9] = p2pSourceAddress[1];
					LogUpdater.LOGGER
							.fine("After edition:\n"
									+ getSDPString(packet.getData(),
											packet.getLength()));
					final StringBuilder st = new StringBuilder();
					st.append(CommandProtocol.SDP);
					// Removing the pad from the length
					st.append(packet.getLength() - 2);
					st.append(CommandProtocol.END_CMD);
					if (flags == 0x87)
						lastSDPPacket = System.nanoTime();
					else
						lastSDPPacket = -1;
					synchronized (serialPortOutputlock) {
						serialOutput.write(st.toString().getBytes("UTF-8"));
						serialOutput.write(packet.getData(), 2,
								packet.getLength() - 2);
						Thread.sleep(75); // On purpose locking this out
					}
				}
			} catch (Exception e) {
				if (!stopRequested)
					e.printStackTrace();
				if (checkIfSerialError(e)) {
					LogUpdater
							.get()
							.appendErrorMessage(
									"Error while writing to the Serial Port. Disconnecting.");
				}
				stopRequested();
				listener.onSerialPortError();
				return;
			} finally {
				serialPort.close();
				datagramSocket.close();
			}
		}
	}

	private class SerialInputThread extends StoppableThread {

		@Override
		public void run() {
			try (final InputStream serialInput = serialPort.getInputStream();) {
				final byte[] buffer = new byte[16384];
				while (!stopRequested) {
					int bytesAvailable = serialInput.available();

					int byteCountToFetch = buffer.length;
					if (byteCountToFetch > bytesAvailable) {
						byteCountToFetch = bytesAvailable;
					}
					int bytesObtained = serialInput.read(buffer, 0,
							byteCountToFetch);

					// bytesAvailable -= bytesObtained;
					// if (bytesAvailable > 0) {
					// byteCountToFetch = buffer.length - bytesObtained;
					// if (byteCountToFetch > bytesAvailable) {
					// byteCountToFetch = bytesAvailable;
					// }
					// serialInput.read(buffer, buffer.length - bytesObtained,
					// byteCountToFetch);
					// }
					for (int i = 0; i < bytesObtained; ++i) {
						try {
							bytesReceived.put(buffer[i]);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
					}

				}
			} catch (IOException e) {
				e.printStackTrace();
				if (checkIfSerialError(e)) {
					LogUpdater
							.get()
							.appendErrorMessage(
									"Error while reading from the Serial Port. Disconnecting.");
					PipeThread.this.stopRequested();
					listener.onSerialPortError();
					return;
				}
			}
		}
	}

	private class InputProcessingThread extends StoppableThread {

		@Override
		public void run() {
			try {

				byte[] buffer = new byte[MAXIMUM_PACKET_SIZE];
				byte[] bufferPacket = new byte[MAXIMUM_PACKET_SIZE];
				// Create a packet to receive data into the buffer
				DatagramPacket packet = new DatagramPacket(bufferPacket,
						bufferPacket.length);
				while (!stopRequested) {
					int firstByte = bytesReceived.take();
					char headerByte = (char) (firstByte & 0x7f);
					switch (headerByte) {
					case CommandProtocol.RECEIVE_PACKET: {
						readBlocking(bytesReceived, buffer, 5);
						final StringBuilder st = new StringBuilder(
								"Received a packet: header: ");
						st.append(String.format("0x%02X", buffer[0]));
						st.append(", address: ");
						st.append(String.format("0x%08X",
								ByteArrayUtils.byteArrayToInt(buffer, 1)));
						LogUpdater.get().appendMessage(st.toString());
					}
						break;
					case CommandProtocol.SENT_PACKET_PAYLOAD:
					case CommandProtocol.RECEIVE_PACKET_PAYLOAD: {
						readBlocking(bytesReceived, buffer, 9);
						final StringBuilder st = new StringBuilder();
						if (headerByte == CommandProtocol.RECEIVE_PACKET_PAYLOAD)
							st.append("Received a packet: header: ");
						else
							st.append("uC sent a packet: header: ");
						st.append(String.format("0x%02X", buffer[0]));
						st.append(", address: ");
						st.append(String.format("0x%08X",
								ByteArrayUtils.byteArrayToInt(buffer, 1)));
						st.append(", payload: ");
						st.append(String.format("0x%08X",
								ByteArrayUtils.byteArrayToInt(buffer, 5)));
						LogUpdater.get().appendMessage(st.toString());
					}
						break;
					case CommandProtocol.RECEIVE_SDP:
						int length = bytesReceived.take();
						length |= (firstByte & 0x80) << 1;
						if (length > MAXIMUM_PACKET_SIZE) {
							LogUpdater.get().appendErrorMessage(
									"SDP too big! " + length);
						}
						readBlocking(bytesReceived, buffer, length);
						final long timeTaken;
						if (lastSDPPacket != -1) {
							timeTaken = System.nanoTime() - lastSDPPacket;
							lastSDPPacket = -1;
							if (timeTaken > 10000000) {
								LogUpdater.get().appendErrorMessage(
										"Taken too long: " + timeTaken);
							}
						} else {
							timeTaken = -1;
						}
						int currentTag = buffer[1] & 0x00ff;
						final SocketAddress ad = IPTag
								.getSocketAddress(currentTag);
						bufferPacket[0] = bufferPacket[1] = 0;
						System.arraycopy(buffer, 0, bufferPacket, 2, length);
						if (ad != null) {
							packet.setData(bufferPacket);
							packet.setLength(length + 2);
							LogUpdater.get().appendMessage(
									"Received a SDP packet from SpiNNaker with length "
											+ length + ". Sending to "
											+ ad.toString());
							if (timeTaken != -1) {
								LogUpdater.get().appendMessage(
										"Time taken:" + timeTaken + "ns. ");
							}
							packet.setSocketAddress(ad);
							datagramSocket.send(packet);
						} else {
							LogUpdater
									.get()
									.appendErrorMessage(
											"Received a SDP packet from SpiNNaker.\nNo entry in the IPTag table. Dropping SDP packet.");
						}
						final String sdpString = getSDPString(bufferPacket,
								length + 2);
						if (DEBUG)
							LogUpdater.get().appendMessage(sdpString);
						else
							LogUpdater.LOGGER.config(sdpString);
						break;
					default:
						LogUpdater.get().appendErrorMessage(
								"Received a strange character from uC: "
										+ String.format("0x%02X",
												firstByte & 0xFF));
						break;
					}
				}
			} catch (IOException e) {
				e.printStackTrace();
				if (checkIfSerialError(e)) {
					LogUpdater
							.get()
							.appendErrorMessage(
									"Error while reading from the Serial Port. Disconnecting.");
					PipeThread.this.stopRequested();
					listener.onSerialPortError();
					return;
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
				LogUpdater
						.get()
						.appendErrorMessage(
								"Error while reading from the Serial Port. Disconnecting.");
				PipeThread.this.stopRequested();
				listener.onSerialPortError();
				return;
			}
		}
	}

	private class BootOutputThread extends StoppableThread {

		private final static byte FF_START = 1;
		private final static byte FF_DATA = 3;
		private final static byte FF_CONTROL = 5;
		private DatagramSocket datagramSocket;
		private int nextNeighbourId = 1;

		@Override
		public void run() {
			try (final OutputStream serialOutput = serialPort.getOutputStream();) {
				// Create a buffer to read datagrams into. If a
				// packet is larger than this buffer, the
				// excess will simply be discarded!
				byte[] buffer = new byte[65535];
				byte[] bootBuffer = new byte[16384];
				int currentOffset = 0;

				datagramSocket = new DatagramSocket(BOOT_PORT);
				// Create a packet to receive data into the buffer
				DatagramPacket packet = new DatagramPacket(buffer,
						buffer.length);
				while (!stopRequested) {
					datagramSocket.receive(packet);
					final byte[] packetData = packet.getData();
					if (packetData[5] == FF_START) {
						currentOffset = 0;
						LogUpdater.get().appendMessage(
								"Started receiving boot image");
					} else if (packetData[5] == FF_DATA) {
						LogUpdater.get().appendMessage(
								"Received a boot packet, size: "
										+ (packet.getLength() - 18));
						ByteArrayUtils.copyFromTo(packetData, 18,
								packet.getLength() - 18, bootBuffer,
								currentOffset);
						currentOffset += packet.getLength() - 18;
					} else if (packetData[5] == FF_CONTROL) {
						LogUpdater.get().appendMessage(
								"Finished receiving boot image");
						LogUpdater.get().appendMessage(
								"Boot size: " + currentOffset);
						LogUpdater.get().appendMessage(
								"Setting P2P address "
										+ String.format("0x%04X", P2P_ADDRESS));

						final StringBuilder st = new StringBuilder();
						st.append(CommandProtocol.BOOT);
						st.append(currentOffset);
						st.append(CommandProtocol.END_CMD);

						synchronized (serialPortOutputlock) {
							serialOutput.write(CommandProtocol.P2P_ADDRESS);
							serialOutput.write(
									ByteBuffer.allocate(4)
											.order(ByteOrder.LITTLE_ENDIAN)
											.putInt(P2P_ADDRESS).array(), 0, 2);
							serialOutput.write(st.toString().getBytes("UTF-8"));
							serialOutput.write(bootBuffer, 0, currentOffset);
						}
						try {
							Thread.sleep(2000);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}

						LogUpdater.get().appendMessage("Configuring P2P");
						sendPacket(CommandProtocol.NN_PAYLOAD, NN_P2PC_KEY
								+ (nextNeighbourId++ << 1), NN_P2PC_PAYLOAD
								+ P2P_ADDRESS);
						nextNeighbourId &= 127;
						sendPacket(CommandProtocol.NN_PAYLOAD, NN_P2PB_KEY
								+ (nextNeighbourId++ << 1), NN_P2PB_PAYLOAD
								+ (P2P_ADDRESS << 16));
						nextNeighbourId &= 127;
						sendPacket(CommandProtocol.NN_PAYLOAD, NN_P2PB_KEY
								+ (nextNeighbourId++ << 1), NN_P2PB_PAYLOAD
								+ (P2P_ADDRESS << 16));
						nextNeighbourId &= 127;
						sendPacket(CommandProtocol.NN_PAYLOAD, NN_P2PB_KEY
								+ (nextNeighbourId++ << 1), NN_P2PB_PAYLOAD
								+ (P2P_ADDRESS << 16));
						nextNeighbourId &= 127;
						try {
							Thread.sleep(500);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
						LogUpdater.get().appendMessage("Configuring Router");
						sendPacket(CommandProtocol.PEEK, 0xf1000000L);
						sendPacket(CommandProtocol.NN_PAYLOAD, NN_RTRC_KEY
								+ (nextNeighbourId++ << 1), NN_RTRC_PAYLOAD);
						nextNeighbourId &= 127;
						sendPacket(CommandProtocol.PEEK, 0xf1000000L);
					}
				}
			} catch (IOException e) {
				if (!stopRequested)
					e.printStackTrace();
				if (checkIfSerialError(e)) {
					LogUpdater
							.get()
							.appendErrorMessage(
									"Error while writing to the Serial Port. Disconnecting.");
					PipeThread.this.stopRequested();
					listener.onSerialPortError();
					return;
				}
			}
		}

		@Override
		public void stopRequested() {
			if (datagramSocket != null)
				datagramSocket.close();
			super.stopRequested();
		}
	}

	private static void readBlocking(BlockingQueue<Byte> bytesReceived,
			byte[] source, int length) throws InterruptedException {
		for (int i = 0; i < length; ++i) {
			source[i] = bytesReceived.take();
		}
	}

	private static String getPacketString(char type, long address) {
		final StringBuilder st = new StringBuilder(
				"Sent a packet to the uC: header: ");
		st.append(type);
		st.append(", address: ");
		st.append(String.format("0x%08X", address));
		return st.toString();
	}

	private static String getPacketString(char type, long address, long payload) {
		final StringBuilder st = new StringBuilder(
				"Sent a packet to the uC: header: ");
		st.append(type);
		st.append(", address: ");
		st.append(String.format("0x%08X", address));
		st.append(", payload: ");
		st.append(String.format("0x%08X", payload));
		return st.toString();
	}

	private static String getSDPString(byte[] packet, int length) {
		final StringBuilder st = new StringBuilder("PAD:");
		st.append(String.format("0x%04X",
				ByteArrayUtils.byteArrayToShort(packet, 0)));
		st.append("\nHeader: Flags:");
		st.append(String.format("0x%02X", packet[2]));
		st.append(", Tag:");
		st.append(String.format("0x%02X", packet[3]));
		st.append(", DestCPU:");
		st.append(String.format("0x%02X", packet[4]));
		st.append(", SrcCPU:");
		st.append(String.format("0x%02X", packet[5]));
		st.append(", DestAddr:");
		st.append(String.format("0x%04X",
				ByteArrayUtils.byteArrayToShort(packet, 6)));
		st.append(", SrcAddr:");
		st.append(String.format("0x%04X",
				ByteArrayUtils.byteArrayToShort(packet, 8)));
		st.append("\nCMD_RC:");
		st.append(String.format("0x%04X",
				ByteArrayUtils.byteArrayToShort(packet, 10)));
		st.append(", Seq:");
		st.append(String.format("0x%04X",
				ByteArrayUtils.byteArrayToShort(packet, 12)));
		st.append(", Arg1:");
		st.append(String.format("0x%08X",
				ByteArrayUtils.byteArrayToInt(packet, 14)));
		st.append(", Arg2:");
		st.append(String.format("0x%08X",
				ByteArrayUtils.byteArrayToInt(packet, 18)));
		st.append(", Arg3:");
		st.append(String.format("0x%08X",
				ByteArrayUtils.byteArrayToInt(packet, 22)));
		if (length > 26) {
			st.append("\nData: ");
			final StringBuilder binary = new StringBuilder();
			boolean allChar = true;
			for (int i = 26; i < length; ++i) {
				binary.append(String.format("%02X ", packet[i]));
				final int c = packet[i];
				if ((c & 0x80) != 0)
					allChar = false;
			}
			if (allChar)
				st.append(new String(packet, 26, length - 26));
			else
				st.append(binary);
		}

		return st.toString();
	}

	private static boolean checkIfSerialError(Exception e) {
		for (StackTraceElement t : e.getStackTrace()) {
			if (t.getClassName().startsWith("gnu.io")) {
				// System.out.println("error with serial port\n exiting.\n");
				return true;
			}
		}
		return false;
	}

	public interface PipeThreadCallbacks {
		public void onSerialPortError();
	}
}
