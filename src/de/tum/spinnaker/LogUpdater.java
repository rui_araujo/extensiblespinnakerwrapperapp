package de.tum.spinnaker;

import java.awt.Color;
import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import javax.swing.SwingUtilities;
import javax.swing.text.BadLocationException;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;

public class LogUpdater {

	public final static Logger LOGGER = Logger.getLogger("SpiNNaker Wrapper");
	private static LogUpdater INSTANCE;

	public static LogUpdater get() {
		if (INSTANCE == null)
			throw new RuntimeException("");
		return INSTANCE;
	}

	public static LogUpdater init(StyledDocument log) {
		if (INSTANCE == null) {
			INSTANCE = new LogUpdater(log);
			try {
				final FileHandler fh = new FileHandler("spinnaker.log", false);
				fh.setFormatter(new SimpleFormatter());
				LOGGER.addHandler(fh);
				LOGGER.setLevel(Level.ALL);
			} catch (SecurityException | IOException e) {
				e.printStackTrace();
			}
		}
		return INSTANCE;
	}

	private final SimpleAttributeSet errorStyle;
	private final StyledDocument log;

	private LogUpdater(StyledDocument log) {
		this.log = log;
		errorStyle = new SimpleAttributeSet();
		StyleConstants.setForeground(errorStyle, Color.RED);
	}

	public void appendMessage(final String msg) {
		LOGGER.info(msg);
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				try {
					log.insertString(log.getLength(), msg + "\n", null);
				} catch (BadLocationException e) {
					e.printStackTrace();
				}
			}
		});
	}

	public void appendErrorMessage(final String msg) {
		LOGGER.warning(msg);
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				try {
					log.insertString(log.getLength(), msg + "\n", errorStyle);
				} catch (BadLocationException e) {
					e.printStackTrace();
				}
			}
		});
	}

}
