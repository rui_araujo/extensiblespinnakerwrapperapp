package de.tum.spinnaker;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public final class ByteArrayUtils {
	private ByteArrayUtils() {
	}

	public static int byteArrayToInt(byte[] source, int offsetSrc) {
		final byte[] dest = new byte[4];
		for (int i = 0; i < 4; ++i) {
			dest[i] = source[i + offsetSrc];
		}
		return ByteBuffer.wrap(dest).order(ByteOrder.LITTLE_ENDIAN).getInt();
	}

	public static short byteArrayToShort(byte[] source, int offsetSrc) {
		final byte[] dest = new byte[2];
		for (int i = 0; i < 2; ++i) {
			dest[i] = source[i + offsetSrc];
		}
		return ByteBuffer.wrap(dest).order(ByteOrder.LITTLE_ENDIAN).getShort();
	}

	public static void copyFromTo(byte[] source, int offsetSrc, int lengthSrc,
			byte[] dest, int offsetDest) {
		byte tmp;
		if (lengthSrc % 4 != 0)
			throw new RuntimeException("it should be an array of int");
		for (int i = offsetSrc; i < (offsetSrc + lengthSrc); i++) {
			dest[offsetDest++] = source[i];
			// Correcting endianess
			if (offsetDest % 4 == 0) {
				tmp = dest[offsetDest - 1];
				dest[offsetDest - 1] = dest[offsetDest - 4];
				dest[offsetDest - 4] = tmp;
				tmp = dest[offsetDest - 2];
				dest[offsetDest - 2] = dest[offsetDest - 3];
				dest[offsetDest - 3] = tmp;
			}
		}
	}

}
